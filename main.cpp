#include <stdio.h>

using namespace std;

int main()
{
    int n, i;
    scanf("%d", &n);

    for (i=-999; i<=-101; ++i)
    {
        printf("%d, ", i);
    }
    printf("%d", n);
    return 0;
}
